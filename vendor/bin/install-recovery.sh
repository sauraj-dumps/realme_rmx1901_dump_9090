#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:2951025f7806842ea4f3c3dd85cb4485150bf6cc; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:a1674383ad9883fb027bfbbd74a39526a38965f8 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:2951025f7806842ea4f3c3dd85cb4485150bf6cc && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
